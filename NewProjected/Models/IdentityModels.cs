﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentitySample.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        static ApplicationDbContext()
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        public DbSet<NewProjected.Models.Venue> Venue { get; set; }
        public DbSet<NewProjected.Models.Vehicles> Vehicles { get; set; }
        public DbSet<NewProjected.Models.Tents> Tents { get; set; }
        public DbSet<NewProjected.Models.Order> Order { get; set; }
        public DbSet<NewProjected.Models.BookingItems> BookingItems { get; set; }
        public DbSet<NewProjected.Models.Employees> Employees { get; set; }
        public DbSet<NewProjected.Models.ComboDeals> ComboDeals { get; set; }
        public DbSet<NewProjected.Models.Promotions> Promotions { get; set; }
        public DbSet<NewProjected.Models.Backery> Backeries { get; set; }
        public DbSet<NewProjected.Models.Event> events { get; set; }
        public DbSet<NewProjected.Models.Manual> Manual { get; set; }
    }
}