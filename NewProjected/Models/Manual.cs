﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewProjected.Models
{
    public class Manual
    {
        [Key]
        [Display(Name = "ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Pdf Name")]
        public string PdfName { get; set; }

        [Display(Name = "pdf file")]
        public byte[] pdf { get; set; }
    }
}