﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace NewProjected.Models
{
    public class Venue
    {
        [Key]
        public int VenueID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        [Display(Name = "Number of Occupants")]
        public int occupantsNumber { get; set; }
        public double Deposit { get; set; }

        [Display(Name = "Cost per day")]
        public double PricePerDay { get; set; }
        [Display(Name = "Picture")]
        public byte[] Pic { get; set; }
    }
    public class Vehicles
    {
        [Key]
        public int VehicleID { get; set; }
        public int Quantity { get; set; }
        [Required]
        [Display(Name = "Vehicle name")]
        public string CarName { get; set; }
        [Display(Name = "Model")]
        public string Model { get; set; }
        [Display(Name = "Number of seats")]
        public string PassangerNum { get; set; }
        public string Colour { get; set; }
        [Display(Name = "Number Plate")]
        public string numberPlate { get; set; }
        public double Deposit { get; set; }
        [Display(Name = "Price per Day")]
        public double PricePerDay { get; set; }
       
        [Display(Name = "Picture")]
        public byte[] Pic { get; set; }
    }

    public class Tents
    {
        [Key]
        public int TentId { get; set; }
        public int Quantity { get; set; }
        [Display(Name = "Tent Name")]
        public string TentName { get; set; }
        [Display(Name = "Number of Poles")]
        public string NumPoles { get; set; }
        [Display(Name = "Deposit")]
        public double Deposit { get; set; }
        [Display(Name = "Cost  per day")]
        public double PriceEachDay { get; set; }

        [Display(Name = "Picture")]
        public byte[] Pic { get; set; }
    }
    public class Order
    {
        [Key]
        public int OrderID { get; set; }
        public string UserID { get; set; }
        public string Status { get; set; }
        [Display(Name = "Total Cost")]
        public double OrderTotal { get; set; }
    }
    public class Event
    {
        [DisplayName("Event Type")]
        [Key]
        public int EventID { get; set; }
        [DisplayName("Event Name")]
        public string EventName { get; set; }
        public ICollection<BookingItems> BookingItems { get; set; }

    }
    public class BookingItems
    {
        [Key]
        public int BookingID { get; set; }
        public int OrderID { get; set; }
        public string UserID { get; set; }
        [Display(Name = "Cake Message")]
        public string CakeMessage { get; set; }
        public int EventID { get; set; }
        public virtual Event Event { get; set; }
        public int Quantity { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        [Display(Name = "Pick up Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime PickupDate { get; set; }
        [Display(Name = "End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime ReturnDate { get; set; }
        public double Deposit { get; set; }
        [Display(Name = "Rental Price")]
        public double Rental { get; set; }

    }
}
