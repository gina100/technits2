﻿using IdentitySample.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NewProjected.Models
{
    public class Combo
    {
        public int ComboID { get; set; }
        [Display(Name = "Combo Name")]
        [Required]
        public string ComboName { get; set; }
        [Display(Name = "Discount Percentage")]
        public double PromoDiscount { get; set; }
        [Display(Name = "Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
        [Display(Name = "Total Discount")]
        public double TotalDiscount { get; set; }
        public List<Resources> Resources { get; set; }
        private ApplicationDbContext db = new ApplicationDbContext();

        private int getItemQuantity(int itemId, string itemName)
        {
            int q = 0;

            var VehiList = db.Vehicles.ToList();
            var TentsList = db.Tents.ToList();
            var VenueList = db.Venue.ToList();

            if (0 < VehiList.Where(p => p.VehicleID == itemId && p.CarName == itemName).Count())
            {
                q = VehiList.Where(p => p.VehicleID == itemId && p.CarName == itemName).Select(l => l.Quantity).FirstOrDefault();
            }

            if (0 < TentsList.Where(p => p.TentId == itemId && p.TentName == itemName).Count())
            {
                q = TentsList.Where(p => p.TentId == itemId && p.TentName == itemName).Select(l => l.Quantity).FirstOrDefault();
            }

            if (0 < VenueList.Where(p => p.VenueID == itemId && p.Name == itemName).Count())
            {
                q = 1;
            }

            return q;
        }

        public string checkQuantity(int itemId,string itemName,DateTime startDate, DateTime endDateTime,int quantity)
        {
            var flag = "Available";
            var bookedItemQuantityList = db.BookingItems.Where(l => l.ItemId == itemId && l.ItemName == itemName && l.PickupDate >= startDate && l.ReturnDate <= endDateTime).Select(p => p.Quantity).ToList();
            var bookedItemQuantity = 0;

            foreach (var it in bookedItemQuantityList) 
            {
                bookedItemQuantity += it;
            }

            if (quantity + bookedItemQuantity > getItemQuantity(itemId, itemName))
            {
                var leftQuantity = getItemQuantity(itemId, itemName) - bookedItemQuantity;
                flag = "We have " + leftQuantity + " facilities left!";
            }

            return flag;
        }

        public void saveBooking(BookingItems booking, string UserName) 
        {
            var price = db.Tents.Where(l => l.TentId == booking.ItemId && l.TentName == booking.ItemName).Select(p => p.PriceEachDay).FirstOrDefault() ;
            if (price == 0)
            {
                price = db.Vehicles.Where(l => l.VehicleID == booking.ItemId && l.CarName == booking.ItemName).Select(p => p.PricePerDay).FirstOrDefault() ;
            }
            if (price == 0)
            {
                price = db.Venue.Where(l => l.VenueID == booking.ItemId && l.Name == booking.ItemName).Select(p => p.PricePerDay).FirstOrDefault();
            }
            if (price == 0)
            {
                price = db.Backeries.Where(l => l.CakeID == booking.ItemId && l.Flavour == booking.ItemName).Select(p => p.Price).FirstOrDefault() ;
                booking.Rental = price;
                booking.Deposit = 0;
            }
            if (booking.Rental == 0)
            {
                var noDays = (booking.ReturnDate.Day - booking.PickupDate.Day);
                if (noDays<0) 
                {
                    noDays = noDays * -1;
                }
                booking.Rental = price * noDays;
                booking.Deposit = booking.Rental / 2;
            }
            booking.UserID =UserName ;
            db.BookingItems.Add(booking);
            db.SaveChanges();
        }
    }

    public class Resources
    {
        public int ResourceId { get; set; }
        public bool ResourceChecked { get; set; }
        public string ResourceName { get; set; }
        public string ResourceType { get; set; }
        public double ResourcePrice { get; set; }

        [Display(Name = "Combo Name")]
        public string ComboName { get; set; }
        [Display(Name = "Discount")]
        public double PromoDiscount { get; set; }
        [Display(Name = "Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
    }
}