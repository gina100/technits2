﻿using System;
using System.ComponentModel.DataAnnotations;
namespace NewProjected.Models
{
    public class Employees
    {
        [Key]
        
        public int EmpID { get; set; }
        [Display(Name ="Tittle")]
        public string title { get; set; }
        [Display(Name ="Employee Name")]
        public string EmployeeName { get; set; }
        [Display(Name ="Employee Surname")]
        public string EmpSurname { get; set; }
        public string Gender { get; set; }
        public string Position { get; set; }
        [Display(Name ="Home Adress")]
        public string HomeAdress { get; set; }
        [Display(Name ="Contact number")]
        public string ContactNum { get; set; }
        [Display(Name ="Charge Price")]
        public double EmpCost { get; set; }
    }
    public class Backery
    {
        [Key]
        public int CakeID { get; set; }
        public string Flavour { get; set; }
        public string Colour { get; set; }
        public double Price { get; set; }
        public byte[] Pic { get; set; }
    }
    public class ComboDeals
    {
        [Key]
        public int ComboID  { get; set; }
        [Display(Name ="Combo Name")]
        public string ComboName { get; set; }
        [Display(Name ="Discount")]
        public double PromoDiscount { get; set; }
        [Display(Name ="Total Discount")]
        public double TotalDiscount { get; set; }
    }
    public class Promotions
    {
        [Key]
        public int propID { get; set; }
        public int ResourceID { get; set; }
        public string ResourceName { get; set; }
        public int ComboID { get; set; }
        [Display(Name ="Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [Display(Name ="End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

    }
    public class PromoVM
    {
        public int ComboID { get; set; }
        [Display(Name = "Combo Name")]
        public string ComboName { get; set; }
        public int ResourceID { get; set; }
        public string ResourceName { get; set; }
        public Byte[] Pic { get; set; }
        [Display(Name ="Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [Display(Name ="End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
        [Display(Name = "Original Price")]
        public double OriginalPrice { get; set; }
        [Display(Name = "Promo Price")]
        public double PromoPrice { get; set; }

    }
}