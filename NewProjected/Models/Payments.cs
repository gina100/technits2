﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewProjected.Models
{
    public class Payments
    {
        public int PaymentID { get; set; }
        public string CustomerID { get; set; }
        public int orderID { get; set; }
        public DateTime DateCreated { get; set; }
        [DisplayName("Total Order Amount ")]
        public double AmountPayed { get; set; }
    }
}