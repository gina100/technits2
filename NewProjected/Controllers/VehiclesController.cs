﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IdentitySample.Models;
using NewProjected.Models;

namespace NewProjected.Controllers
{
    public class VehiclesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Vehicles
        public ActionResult Index()
        {
            return View(db.Vehicles.ToList());
        }
        
        public ActionResult PaymentDetails(int id)
        {
            ViewBag.id = id;
            ViewBag.status = db.Order.Where(l => l.OrderID == id).Select(i => i.Status).FirstOrDefault();
            ViewBag.ordertotal = db.Order.Where(l => l.OrderID == id).Select(i => i.OrderTotal).FirstOrDefault();
            ViewBag.UserName = db.Order.Where(l => l.OrderID == id).Select(i => i.UserID).FirstOrDefault();

            Payments payment = new Payments();
            payment.orderID = id;
            payment.AmountPayed= db.Order.Where(l => l.OrderID == id).Select(i => i.OrderTotal).FirstOrDefault();
            return View(payment);
        }

        [Authorize]
        public ActionResult BookingIndex()
        {
            return View(db.BookingItems.ToList().Where(l=>l.UserID==User.Identity.Name&&l.OrderID==0&& (l.CakeMessage == string.Empty || l.CakeMessage == null)));
        } 
        [Authorize]
        public ActionResult BookingCakeIndex()
        {
            var backeries = db.Backeries.ToList();
            var bookingCakes = new List<BookingItems>();
            foreach (var item in backeries)
            {
                var bookedItem = new BookingItems();
                bookedItem = db.BookingItems.Where(l => l.ItemId == item.CakeID && l.ItemName == item.Flavour&&l.OrderID==0).FirstOrDefault();
                if (bookedItem!=null) 
                {
                    bookingCakes.Add(bookedItem);
                }
            }
            return View(bookingCakes);
        }
        [Authorize]
        [ChildActionOnly]
        public ActionResult CakeDetails(int id)
        {
            var backeries = db.Backeries.ToList();
            var bookingCakes = new List<BookingItems>();
            foreach (var item in backeries)
            {
                var bookedItem = new BookingItems();
                bookedItem = db.BookingItems.Where(l => l.ItemId == item.CakeID && l.ItemName == item.Flavour&&l.OrderID==id).FirstOrDefault();
                if (bookedItem!=null) 
                {
                    bookingCakes.Add(bookedItem);
                }
            }
            return PartialView(bookingCakes);
        }

        [Authorize]
        [ChildActionOnly]
        public ActionResult BookingItems(int id)
        {
            ViewBag.id = id;
            ViewBag.status = db.Order.Where(l => l.OrderID == id).Select(i => i.Status).FirstOrDefault();

            var backeries = db.Backeries.ToList();
            var bookingCakes = db.BookingItems.Where(l=>(l.CakeMessage==string.Empty|| l.CakeMessage == null )&& l.OrderID==id).ToList();
           
            return PartialView(bookingCakes);
        }
        [Authorize]
        public ActionResult BookedItems(int id)
        {
            ViewBag.id = id;
            ViewBag.status = db.Order.Where(l => l.OrderID == id).Select(i => i.Status).FirstOrDefault();

            var backeries = db.Backeries.ToList();
            var bookingCakes = new List<BookingItems>();
            foreach (var item in backeries)
            {
                var bookedItem = new BookingItems();
                bookedItem = db.BookingItems.Where(l => l.ItemId != item.CakeID && l.ItemName != item.Flavour && l.OrderID != id).FirstOrDefault();
                bookingCakes.Add(bookedItem);
            }
            return View(bookingCakes);
        }

        [Authorize]
        public ActionResult OrderDetails()
        {
            var list = db.Order.ToList();

            if (!User.IsInRole("Admin")) 
            {
                list = list.Where(l => l.UserID == User.Identity.Name).ToList();
            }
            return View(list);
        }


        [Authorize]
        public ActionResult NewOrder()
        {
            var newOrder = new Order();
            newOrder.UserID = User.Identity.Name;
            newOrder.Status = "Waiting for Approval";
            newOrder.OrderTotal = db.BookingItems.Where(l => l.UserID == User.Identity.Name && l.OrderID == 0).Select(l => l.Rental).Sum();
            db.Order.Add(newOrder);
            db.SaveChanges();

            var bookings = db.BookingItems.Where(o => o.UserID == User.Identity.Name && o.OrderID == 0);
            foreach (var item in bookings)
            {
                item.OrderID = newOrder.OrderID;
                db.Entry(item).State = EntityState.Modified;
            }
            db.SaveChanges();
            return RedirectToAction("OrderDetails");
        }

        // GET: Vehicles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicles vehicles = db.Vehicles.Find(id);
            if (vehicles == null)
            {
                return HttpNotFound();
            }
            return View(vehicles);
        }

        // GET: Vehicles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vehicles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "Pic")]Vehicles vehicles)
        {
            if (ModelState.IsValid)
            {
                byte[] Pic = null;
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase poImgFile = Request.Files["Pic"];

                    using (var binary = new BinaryReader(poImgFile.InputStream))
                    {
                        Pic = binary.ReadBytes(poImgFile.ContentLength);
                    }
                }
                vehicles.Pic = Pic;
                vehicles.Deposit = vehicles.PricePerDay / 2;
                db.Vehicles.Add(vehicles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vehicles);
        }
        public FileContentResult UserPhotos(int vehicleId)
        {
            if (vehicleId == 0)
            {
                string fileName = HttpContext.Server.MapPath(@"~/Images/noImg.png");

                byte[] imageData = null;
                FileInfo fileInfo = new FileInfo(fileName);
                long imageFileLength = fileInfo.Length;
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                imageData = br.ReadBytes((int)imageFileLength);

                return File(imageData, "image/png");

            }
            else
            {
                var pic = db.Vehicles.Where(k => k.VehicleID == vehicleId).Select(k => k.Pic).FirstOrDefault();
                return new FileContentResult(pic, "image/jpeg");

            }
        }

        // GET: Vehicles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicles vehicles = db.Vehicles.Find(id);
            if (vehicles == null)
            {
                return HttpNotFound();
            }
            return View(vehicles);
        }

        // POST: Vehicles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VehicleID,CarName,Model,PassangerNum,Colour,numberPlate,Deposit,PricePerDay,Pic")] Vehicles vehicles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehicles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vehicles);
        }

        // GET: Vehicles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicles vehicles = db.Vehicles.Find(id);
            if (vehicles == null)
            {
                return HttpNotFound();
            }
            return View(vehicles);
        }

        public ActionResult Approve(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var Order = db.Order.Where(l=>l.OrderID==id).FirstOrDefault();
            Order.Status = "Approved";
            db.Entry(Order).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("OrderDetails");
        }
        public ActionResult Decline(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var Order = db.Order.Where(l=>l.OrderID==id).FirstOrDefault();
            Order.Status = "Declined";
            db.Entry(Order).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("OrderDetails");
        }

        // POST: Vehicles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vehicles vehicles = db.Vehicles.Find(id);
            db.Vehicles.Remove(vehicles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
