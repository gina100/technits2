﻿using System;
using System.Linq;
using System.Web.Mvc;
using PayFast;
using PayFast.AspNet;
using System.Configuration;
using System.Threading.Tasks;
using System.Net;
using NewProjected.Models;
using System.IO;
using Rotativa;
using IdentitySample.Models;
using System.Data.Entity;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Text;
using System.Net.Mail;

namespace NewProjected.MVC5.Controllers
{
    public class PaymentController : Controller
    {
        #region Fields
        private ApplicationDbContext db = new ApplicationDbContext();

        private readonly PayFastSettings payFastSettings;
         Order order = new Order();

        #endregion Fields

        #region Constructor

        public PaymentController()
        {
            this.payFastSettings = new PayFastSettings();
            this.payFastSettings.MerchantId = ConfigurationManager.AppSettings["MerchantId"];
            this.payFastSettings.MerchantKey = ConfigurationManager.AppSettings["MerchantKey"];
            this.payFastSettings.PassPhrase = ConfigurationManager.AppSettings["PassPhrase"];
            this.payFastSettings.ProcessUrl = ConfigurationManager.AppSettings["ProcessUrl"];
            this.payFastSettings.ValidateUrl = ConfigurationManager.AppSettings["ValidateUrl"];
            this.payFastSettings.ReturnUrl = ConfigurationManager.AppSettings["ReturnUrl"];
            this.payFastSettings.CancelUrl = ConfigurationManager.AppSettings["CancelUrl"];
            this.payFastSettings.NotifyUrl = ConfigurationManager.AppSettings["NotifyUrl"];
        }

        #endregion Constructor

        #region Methods

        public ActionResult Create(int id)
        {
            Payments payment = new Payments();
            payment.orderID = id;
            ViewBag.id = id;
            return View(payment);
        }
        [HttpPost]
        public ActionResult Create(Payments payment)
        {
            if(ModelState.IsValid)
            { var kl = db.Order.Where(k => k.OrderID == payment.orderID).FirstOrDefault();
                if (kl.OrderTotal >=payment.AmountPayed)
                {
                    payment.AmountPayed = kl.OrderTotal;
                    kl.OrderTotal = kl.OrderTotal - payment.AmountPayed;
                    kl.Status = "Payed";
                    db.Entry(kl).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("OnceOff", payment);
                }
                else
                {
                    ModelState.AddModelError("", "Your Amount cannot be greater than :" + kl);
                    return View(payment);
                }
            }
            return View(payment);
        }

        public ActionResult PrintReport(int FeeId)
        {
            var report = new ActionAsPdf("Index", new { FeeId = FeeId });
            return report;
        }

        public ActionResult OnceOff(Payments payment)
        {
            var onceOffRequest = new PayFastRequest(this.payFastSettings.PassPhrase);
            //var callbackUrl = Url.Action("Create", "Parents",null, protocol: Request.Url.Scheme);
            var callbackUrl = Url.Action("Return", "Payment", payment);
            CreatePdf(payment.orderID);
            // Merchant Details
            onceOffRequest.merchant_id = this.payFastSettings.MerchantId;
            onceOffRequest.merchant_key = this.payFastSettings.MerchantKey;
            onceOffRequest.SetPassPhrase(this.payFastSettings.PassPhrase);
            //onceOffRequest.return_url = this.payFastSettings.ReturnUrl;
            onceOffRequest.return_url = this.payFastSettings.ReturnUrl;

            onceOffRequest.cancel_url = this.payFastSettings.CancelUrl;
            //onceOffRequest.notify_url = this.payFastSettings.NotifyUrl;

            // Buyer Details
            onceOffRequest.email_address = "sbtu01@payfast.co.za";

            // Transaction Details 4e8d1379-688c-4d12-8d88-e9ec6078358f
            onceOffRequest.m_payment_id = "8d00bf49-e979-4004-228c-08d452b86380";
            onceOffRequest.amount = payment.AmountPayed;
            onceOffRequest.item_name = "Once off option";
            onceOffRequest.item_description = "Some details about the once off payment";

            // Transaction Options
            onceOffRequest.email_confirmation = true;
            onceOffRequest.confirmation_address = "suphiwok@gmail.com";
            
            var redirectUrl = $"{this.payFastSettings.ProcessUrl}{onceOffRequest.ToString()}";
            return Redirect(redirectUrl);
        }
        
        public ActionResult Return()
        {
           
            return View();
        }

        public ActionResult Cancel()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Notify([ModelBinder(typeof(PayFastNotifyModelBinder))]PayFastNotify payFastNotifyViewModel)
        {
            payFastNotifyViewModel.SetPassPhrase(this.payFastSettings.PassPhrase);

            var calculatedSignature = payFastNotifyViewModel.GetCalculatedSignature();

            var isValid = payFastNotifyViewModel.signature == calculatedSignature;

            System.Diagnostics.Debug.WriteLine($"Signature Validation Result: {isValid}");

            // The PayFast Validator is still under developement
            // Its not recommended to rely on this for production use cases
            var payfastValidator = new PayFastValidator(this.payFastSettings, payFastNotifyViewModel, IPAddress.Parse(this.HttpContext.Request.UserHostAddress));

            var merchantIdValidationResult = payfastValidator.ValidateMerchantId();

            System.Diagnostics.Debug.WriteLine($"Merchant Id Validation Result: {merchantIdValidationResult}");

            var ipAddressValidationResult = payfastValidator.ValidateSourceIp();

            System.Diagnostics.Debug.WriteLine($"Ip Address Validation Result: {merchantIdValidationResult}");

            // Currently seems that the data validation only works for successful payments
            if (payFastNotifyViewModel.payment_status == PayFastStatics.CompletePaymentConfirmation)
            {
                var dataValidationResult = await payfastValidator.ValidateData();

                System.Diagnostics.Debug.WriteLine($"Data Validation Result: {dataValidationResult}");
            }

            if (payFastNotifyViewModel.payment_status == PayFastStatics.CancelledPaymentConfirmation)
            {
                System.Diagnostics.Debug.WriteLine($"Subscription was cancelled");
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public ActionResult Error()
        {
            return View();
        }

        private ApplicationDbContext applicationDb = new ApplicationDbContext();
        [Authorize]
        public void CreatePdf(int id)
        {
            var order = applicationDb.Order.Where(l => l.OrderID == id).FirstOrDefault();
            MemoryStream workStream = new MemoryStream();
            StringBuilder status = new StringBuilder("");
            DateTime dTime = DateTime.Now;
            //file name to be created 
            string strPDFFileName = string.Format("OderDetails.pdf");
            iTextSharp.text.Document doc = new iTextSharp.text.Document();
            doc.SetMargins(0f, 0f, 0f, 0f);
            //Create PDF Table with 5 columns
            PdfPTable tableLayout = new PdfPTable(6);
            doc.SetMargins(0f, 0f, 0f, 0f);

            string strAttachment = Server.MapPath("~/Downloads/" + strPDFFileName);
            PdfWriter.GetInstance(doc, workStream).CloseStream = false;
            doc.Open();
            //  string imagepath = Server.MapPath(Url.Content("~/Content/Images/logo_type.jpg"));
            doc.Add(new Paragraph(""));

            //Image gif = Image.GetInstance(imagepath);

            //doc.Add(gif);
            Chunk c1 = new Chunk("Order Num : " + order.OrderID);
            DateTime dt = DateTime.Now;
            Chunk chunk = new Chunk("                                          Date Ordered : " + dt.ToString(), FontFactory.GetFont("dax-black"));
            doc.Add(new Paragraph("Contact Details"));
            chunk.SetUnderline(0.5f, -1.5f);

            doc.Add(c1);
            doc.Add(chunk);

            doc.Add(Add_Content_To_PDF(tableLayout, order.OrderID, order));

            // Closing the document
            doc.Close();

            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;
            MemoryStream workStream2 = new MemoryStream();
            workStream2 = workStream;
            MailMessage msg = new MailMessage();


            //string fileName = Path.GetFileName(fileUploader.FileName);

            msg.Attachments.Add(new Attachment(workStream, strPDFFileName));

            //}
            msg.From = new MailAddress("abantwanaweb@gmail.com");
            //msg.To.Add(new MailAddress("abantwanaweb@gmail.com"));
            msg.To.Add(new MailAddress(order.UserID));
            msg.Subject = "New Order";
            msg.Body = "Please Find Attached Order details";
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", Convert.ToInt32(587));
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("abantwanaweb@gmail.com", "$Account2020");
            smtpClient.Credentials = credentials;
            smtpClient.EnableSsl = true;
            smtpClient.Send(msg);

           // return File(workStream2, "application/pdf", strPDFFileName);

        }



        protected PdfPTable Add_Content_To_PDF(PdfPTable tableLayout, int id, Order order)
        {

            float[] headers = { 35, 35, 35, 35, 35, 35 };  //Header Widths
            tableLayout.SetWidths(headers);        //Set the pdf headers
            tableLayout.WidthPercentage = 100;       //Set the PDF File witdh percentage
            tableLayout.HeaderRows = 1;
            //Chunk c1 = new Chunk("A chunk represents an isolated string. ");

            tableLayout.AddCell(new PdfPCell(new Phrase("Order Details", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(0, 0, 0)))) { Colspan = 12, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });


            ////Add header
            AddCellToHeader(tableLayout, "Item Code");
            AddCellToHeader(tableLayout, "Item Name");
            AddCellToHeader(tableLayout, "Deposit");
            AddCellToHeader(tableLayout, "Rental Price");
            AddCellToHeader(tableLayout, "Start Date");
            AddCellToHeader(tableLayout, "End Date");

            ////Add body

            //Employee em = new Employee();
            //Student Studentz = new Student();
            // List<Employee> el = new List<Employee>();
            var orderB = applicationDb.BookingItems.Where(l => l.OrderID == id).ToList();

            foreach (var item in orderB)
            {

                //decimal subtotal = item.Quantity * item.Price;
                AddCellToBody(tableLayout, item.ItemName.Substring(0, 3) + item.ItemId.ToString());
                AddCellToBody(tableLayout, item.ItemName);
                //AddCellToBody(tableLayout, item.i);
                AddCellToBody(tableLayout, "R" + item.Deposit.ToString());
                AddCellToBody(tableLayout, "R" + item.Rental.ToString());
                AddCellToBody(tableLayout, item.PickupDate.ToString("dd/MM/yyyy"));
                AddCellToBody(tableLayout, item.ReturnDate.ToString("dd/MM/yyyy"));


            }


            tableLayout.AddCell(new PdfPCell(new Phrase("Total : R" + order.OrderTotal, new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(0, 0, 0)))) { Colspan = 12, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });

            return tableLayout;
        }

        // Method to add single cell to the Header
        private static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {

            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.WHITE))) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = new iTextSharp.text.BaseColor(65, 105, 225) });
        }

        // Method to add single cell to the body
        private static void AddCellToBody(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255) });
        }

        #endregion Methods
    }
}
