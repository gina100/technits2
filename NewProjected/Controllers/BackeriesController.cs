﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IdentitySample.Models;
using NewProjected.Models;

namespace NewProjected.Controllers
{
    public class BackeriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Backeries
        public ActionResult Index()
        {
            return View(db.Backeries.ToList());
        }

        // GET: Backeries/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Backery backery = db.Backeries.Find(id);
            if (backery == null)
            {
                return HttpNotFound();
            }
            return View(backery);
        }

        // GET: Backeries/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Backeries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "Pic")] Backery backery)
        {
            if (ModelState.IsValid)
            {
                byte[] Pic = null;
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase poImgFile = Request.Files["Pic"];

                    using (var binary = new BinaryReader(poImgFile.InputStream))
                    {
                        Pic = binary.ReadBytes(poImgFile.ContentLength);
                    }
                }
                backery.Pic = Pic;
                db.Backeries.Add(backery);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(backery);
        }

        // GET: Backeries/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Backery backery = db.Backeries.Find(id);
            if (backery == null)
            {
                return HttpNotFound();
            }
            return View(backery);
        }

        // POST: Backeries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CakeID,Flavour,Colour")] Backery backery)
        {
            if (ModelState.IsValid)
            {
                db.Entry(backery).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(backery);
        }

        // GET: Backeries/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Backery backery = db.Backeries.Find(id);
            if (backery == null)
            {
                return HttpNotFound();
            }
            return View(backery);
        }

        // POST: Backeries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Backery backery = db.Backeries.Find(id);
            db.Backeries.Remove(backery);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
