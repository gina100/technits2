﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IdentitySample.Models;
using NewProjected.Models;

namespace NewProjected.Controllers
{
    public class TentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Tents
        public ActionResult Index()
        {
            return View(db.Tents.ToList());
        }

        // GET: Tents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tents tents = db.Tents.Find(id);
            if (tents == null)
            {
                return HttpNotFound();
            }
            return View(tents);
        }

        // GET: Tents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "Pic")] Tents tents)
        {
            if (ModelState.IsValid)
            {
                byte[] Pic = null;
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase poImgFile = Request.Files["Pic"];

                    using (var binary = new BinaryReader(poImgFile.InputStream))
                    {
                        Pic = binary.ReadBytes(poImgFile.ContentLength);
                    }
                }
                tents.Pic = Pic;
                tents.Deposit = tents.PriceEachDay / 2;
                db.Tents.Add(tents);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tents);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult NewBooking(BookingItems booking)
        {
            if (ModelState.IsValid)
            {
                if (booking.PickupDate < booking.ReturnDate)
                {
                    Combo combo = new Combo();
                    if (combo.checkQuantity(booking.ItemId, booking.ItemName, booking.PickupDate, booking.ReturnDate, booking.Quantity) == "Available")
                    {
                        var backeryList = db.Backeries.ToList();
                        var VehiList = db.Vehicles.ToList();
                        var TentsList = db.Tents.ToList();
                        var VenueList = db.Venue.ToList();

                        var getComboItems = db.ComboDeals.Where(l => l.ComboID == booking.ItemId && l.ComboName == booking.ItemName).FirstOrDefault();

                        if (getComboItems != null)
                        {
                            var getpromItems = db.Promotions.Where(o => o.ComboID == getComboItems.ComboID).ToList();
                            if ((booking.PickupDate >= getpromItems[0].StartDate) && (booking.ReturnDate <= getpromItems[0].EndDate))
                            {


                                foreach (var item in getpromItems)
                                {
                                    booking.ItemId = item.ResourceID;
                                    booking.ItemName = item.ResourceName;
                                    var price = 0.0;

                                    if (0 < backeryList.Where(p => p.CakeID == item.ResourceID && p.Flavour == item.ResourceName).Count())
                                    {
                                        var OriginalPrice = backeryList.Where(p => p.CakeID == item.ResourceID && p.Flavour == item.ResourceName).Select(l => l.Price).FirstOrDefault()*booking.Quantity;
                                        booking.Rental = OriginalPrice - (OriginalPrice / getComboItems.PromoDiscount);
                                    }

                                    if (0 < VehiList.Where(p => p.VehicleID == item.ResourceID && p.CarName == item.ResourceName).Count())
                                    {
                                        var OriginalPrice = VehiList.Where(p => p.VehicleID == item.ResourceID && p.CarName == item.ResourceName).Select(l => l.PricePerDay).FirstOrDefault()*booking.Quantity;
                                        price = OriginalPrice - (OriginalPrice / getComboItems.PromoDiscount);
                                    }

                                    if (0 < TentsList.Where(p => p.TentId == item.ResourceID && p.TentName == item.ResourceName).Count())
                                    {
                                        var OriginalPrice = TentsList.Where(p => p.TentId == item.ResourceID && p.TentName == item.ResourceName).Select(l => l.PriceEachDay).FirstOrDefault()* booking.Quantity;
                                        price = OriginalPrice - (OriginalPrice / getComboItems.PromoDiscount);
                                    }

                                    if (0 < VenueList.Where(p => p.VenueID == item.ResourceID && p.Name == item.ResourceName).Count())
                                    {
                                        var OriginalPrice = VenueList.Where(p => p.VenueID == item.ResourceID && p.Name == item.ResourceName).Select(l => l.PricePerDay).FirstOrDefault()* 1;
                                        price = OriginalPrice - (OriginalPrice / getComboItems.PromoDiscount);
                                    }
                                    if (booking.Rental == 0)
                                    {
                                        booking.Rental = price * (booking.ReturnDate.Day - booking.PickupDate.Day);
                                        booking.Deposit = booking.Rental / 2;
                                    }

                                    booking.UserID = User.Identity.Name;
                                    db.BookingItems.Add(booking);
                                }
                                db.SaveChanges();
                            }
                            else
                            {
                                ViewBag.EventID = new SelectList(db.events, "EventID", "EventName");
                                ModelState.AddModelError(string.Empty, "The Combo promotion only applies within the " + getpromItems[0].StartDate.Date.ToString("dd/MM/yyyy") + " to " + getpromItems[0].EndDate.Date.ToString("dd/MM/yyyy"));
                                return View(booking);
                            }
                        }
                        else
                        {
                            combo.saveBooking(booking, User.Identity.Name);
                        }
                        return RedirectToAction("BookingIndex", "Vehicles");
                    }
                    else
                    {
                        ViewBag.EventID = new SelectList(db.events, "EventID", "EventName");
                        ModelState.AddModelError(string.Empty, combo.checkQuantity(booking.ItemId, booking.ItemName, booking.PickupDate, booking.ReturnDate, booking.Quantity));
                        return View(booking);
                    }
                }
                else
                {
                    ViewBag.EventID = new SelectList(db.events, "EventID", "EventName");
                    ModelState.AddModelError(string.Empty, "The return date cannot be lesser than pick up date !");
                    return View(booking);
                }
            }
                return View(booking);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult NewCake(BookingItems booking)
        {
            if (ModelState.IsValid)
            {
                var cakePrice = db.Backeries.Where(l => l.CakeID == booking.ItemId && l.Flavour == booking.ItemName).Select(l => l.Price).FirstOrDefault();
                if (cakePrice>0) 
                {
                    booking.Rental = cakePrice;
                    booking.ReturnDate = booking.PickupDate;
                    booking.UserID = User.Identity.Name;
                    booking.Quantity = 1;
                    db.BookingItems.Add(booking);
                    db.SaveChanges();
                }
                return RedirectToAction("BookingCakeIndex", "Vehicles");
            }
            else
            {
                ViewBag.EventID = new SelectList(db.events, "EventID", "EventName");
                ModelState.AddModelError(string.Empty, "Cake could not be ordered!");
                return View(booking);
            }
        }
        [Authorize]
        // GET: Tents/Edit/5
        public ActionResult NewBooking(int itemId,string itemName)
        {
            BookingItems items = new BookingItems();
            items.ItemId = itemId;
            items.ItemName = itemName;
            ViewBag.Venue = true;
            ViewBag.EventID = new SelectList(db.events, "EventID", "EventName");
            var VenueList = db.Venue.ToList();
            if (0 < VenueList.Where(p => p.VenueID == itemId&& p.Name == itemName).Count())
            {
                ViewBag.Venue = false;
            }

            return View(items);
        }

        [Authorize]
        // GET: Tents/Edit/5
        public ActionResult NewCake(int itemId, string itemName)
        {
            BookingItems items = new BookingItems();
            items.ItemId = itemId;
            items.ItemName = itemName;
            ViewBag.EventID = new SelectList(db.events, "EventID", "EventName");

            return View(items);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tents tents = db.Tents.Find(id);
            if (tents == null)
            {
                return HttpNotFound();
            }
            return View(tents);
        }

        // POST: Tents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TentId,TentName,NumPoles,diposit,PriceEachDay,Pic")] Tents tents)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tents).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tents);
        }

        // GET: Tents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tents tents = db.Tents.Find(id);
            if (tents == null)
            {
                return HttpNotFound();
            }
            return View(tents);
        }

        public FileContentResult UserPhotos(int tentId)
        {
            if (tentId == 0)
            {
                string fileName = HttpContext.Server.MapPath(@"~/Images/noImg.png");

                byte[] imageData = null;
                FileInfo fileInfo = new FileInfo(fileName);
                long imageFileLength = fileInfo.Length;
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                imageData = br.ReadBytes((int)imageFileLength);

                return File(imageData, "image/png");

            }
            else
            {
                var pic = db.Tents.Where(k=>k.TentId==tentId).Select(k=> k.Pic).FirstOrDefault();
                return new FileContentResult(pic, "image/jpeg");

            }
        }

        // POST: Tents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tents tents = db.Tents.Find(id);
            db.Tents.Remove(tents);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
