﻿using NewProjected.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using IdentitySample.Models;
using Microsoft.Ajax.Utilities;
using Microsoft.Owin.Security.OAuth;

namespace NewProjected.Controllers
{
    public class ComboController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            var cmlist = db.ComboDeals.ToList();
            var promoList = db.Promotions.ToList();
            var comboList = new List<Combo>();
            foreach (var item in cmlist) 
            {
                Combo combo = new Combo();
                combo.ComboID = item.ComboID;
                combo.ComboName = item.ComboName;
                combo.PromoDiscount = item.PromoDiscount;
                combo.EndDate = promoList.Where(o=>o.ComboID==item.ComboID).Select(l => l.EndDate).FirstOrDefault();
                combo.StartDate = promoList.Where(o=>o.ComboID==item.ComboID).Select(l => l.StartDate).FirstOrDefault();
                combo.TotalDiscount = item.TotalDiscount;
                comboList.Add(combo);
            }
            return View(comboList);
        }

        public ActionResult PromoIndex(int id)
        {
            var promoList = db.Promotions.Where(l=>l.ComboID==id).ToList();
            var promoDisc = db.ComboDeals.Where(o => o.ComboID == id).Select(l => l.PromoDiscount).FirstOrDefault();
            ViewBag.Name = db.ComboDeals.Where(o => o.ComboID == id).Select(l => l.ComboName).FirstOrDefault();
            ViewBag.TotPrice = db.ComboDeals.Where(o => o.ComboID == id).Select(l => l.TotalDiscount).FirstOrDefault();
            //var emploList = db.Employees.ToList();
            var backeryList = db.Backeries.ToList();
            var VehiList = db.Vehicles.ToList();
            var TentsList = db.Tents.ToList();
            var VenueList = db.Venue.ToList();
            var comboPromoList = new List<PromoVM>();
            foreach (var item in promoList)
            {
                PromoVM promoVM = new PromoVM();
                promoVM.ComboID = item.ComboID;
                promoVM.ComboName = ViewBag.Name;
                promoVM.ResourceName = item.ResourceName;
                promoVM.StartDate = item.StartDate;
                promoVM.EndDate = item.EndDate;
                if (0< backeryList.Where(p=>p.CakeID==item.ResourceID&&p.Flavour==item.ResourceName).Count()) 
                {
                    promoVM.Pic = backeryList.Where(p => p.CakeID == item.ResourceID && p.Flavour == item.ResourceName).Select(l => l.Pic).FirstOrDefault();
                    promoVM.OriginalPrice = backeryList.Where(p => p.CakeID == item.ResourceID && p.Flavour == item.ResourceName).Select(l => l.Price).FirstOrDefault();
                    promoVM.PromoPrice =promoVM.OriginalPrice-(promoVM.OriginalPrice/promoDisc);
                }

                if (0< VehiList.Where(p=>p.VehicleID==item.ResourceID&&p.CarName==item.ResourceName).Count()) 
                {
                    promoVM.Pic = VehiList.Where(p => p.VehicleID == item.ResourceID && p.CarName == item.ResourceName).Select(l => l.Pic).FirstOrDefault();
                    promoVM.OriginalPrice = VehiList.Where(p => p.VehicleID == item.ResourceID && p.CarName == item.ResourceName).Select(l => l.PricePerDay).FirstOrDefault();
                    promoVM.PromoPrice =promoVM.OriginalPrice-(promoVM.OriginalPrice/promoDisc);
                }

                if (0< TentsList.Where(p=>p.TentId==item.ResourceID&&p.TentName==item.ResourceName).Count()) 
                {
                    promoVM.Pic = TentsList.Where(p => p.TentId == item.ResourceID && p.TentName == item.ResourceName).Select(l => l.Pic).FirstOrDefault();
                    promoVM.OriginalPrice = TentsList.Where(p => p.TentId == item.ResourceID && p.TentName == item.ResourceName).Select(l => l.PriceEachDay).FirstOrDefault();
                    promoVM.PromoPrice =promoVM.OriginalPrice-(promoVM.OriginalPrice/promoDisc);
                }
                
                if (0< VenueList.Where(p=>p.VenueID==item.ResourceID&&p.Name==item.ResourceName).Count()) 
                {
                    promoVM.Pic = VenueList.Where(p => p.VenueID == item.ResourceID && p.Name == item.ResourceName).Select(l => l.Pic).FirstOrDefault();
                    promoVM.OriginalPrice = VenueList.Where(p => p.VenueID == item.ResourceID && p.Name == item.ResourceName).Select(l => l.PricePerDay).FirstOrDefault();
                    promoVM.PromoPrice =promoVM.OriginalPrice-(promoVM.OriginalPrice/promoDisc);
                }

                comboPromoList.Add(promoVM);
            }
            return View(comboPromoList);
        }
        // GET: Combo
        public ActionResult CreateCombo()
        {
            //var combo = new Combo();
            var comboList = new List<Resources>();
            //combo.Resources = comboList;
            var vehicles = db.Vehicles.ToList();
            var employees = db.Employees.ToList();
            var bakery = db.Backeries.ToList();
            var venue = db.Venue.ToList();
            var tents = db.Tents.ToList();
            foreach(var item in vehicles)
            {
                var obj = new Resources();
                obj.ResourceId = item.VehicleID;
                obj.ResourceType = "Vehicle";
                obj.ResourceName = item.CarName;
                obj.ResourcePrice = item.PricePerDay;
                comboList.Add(obj);
            }
            
            foreach(var item in bakery)
            {
                var obj = new Resources();
                obj.ResourceId = item.CakeID;
                obj.ResourceName = item.Flavour;
                obj.ResourceType = "Bakery";
                obj.ResourcePrice = item.Price;
                comboList.Add(obj);
            }
            
            foreach(var item in venue)
            {
                var obj = new Resources();
                obj.ResourceId = item.VenueID;
                obj.ResourceName = item.Name;
                obj.ResourceType = "Venue";
                obj.ResourcePrice = item.PricePerDay;
                comboList.Add(obj);
            }
            
            foreach(var item in tents)
            {
                var obj = new Resources();
                obj.ResourceId = item.TentId;
                obj.ResourceName = item.TentName;
                obj.ResourceType = "Tents";
                obj.ResourcePrice = item.PriceEachDay;
                comboList.Add(obj);
            }
            return View(comboList);
        }

        [HttpPost]
        public ActionResult CreateCombo(List<Resources> rs)
        {
            if (ModelState.IsValid)
            {
                var combo = new ComboDeals();
                combo.ComboName = rs[0].ComboName;
                combo.PromoDiscount = rs[0].PromoDiscount;
                foreach (var item in rs.Where(l => l.ResourceChecked == true))
                {
                    combo.TotalDiscount = combo.TotalDiscount + (item.ResourcePrice-(item.ResourcePrice / rs[0].PromoDiscount));
                }

                    if (combo.TotalDiscount!=0)
                {
                    db.ComboDeals.Add(combo);
                    db.SaveChanges();
                    foreach (var item in rs.Where(l => l.ResourceChecked == true))
                    {
                        var promo = new Promotions();
                        promo.StartDate = rs[0].StartDate;
                        promo.EndDate = rs[0].EndDate;
                        promo.ComboID = combo.ComboID;
                        promo.ResourceID = item.ResourceId;
                        promo.ResourceName= item.ResourceName;
                        db.Promotions.Add(promo);
                        db.SaveChanges();
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Please select at least one resource !");
                    return View(rs);
                }
                return RedirectToAction("Index");
            }

            return View(rs);
        }
    }
}