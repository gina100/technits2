﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IdentitySample.Models;
using NewProjected.Models;

namespace NewProjected.Controllers
{
    public class ManualsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Manuals
        public ActionResult Index()
        {
            return View(db.Manual.ToList());
        }

        // GET: Manuals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Manual manual = db.Manual.Find(id);
            if (manual == null)
            {
                return HttpNotFound();
            }
            return View(manual);
        }

        // GET: Manuals/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Manuals/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "img_upload")] Manual manual)
        {
            if (ModelState.IsValid)
            {
                byte[] Pic = null;
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase poImgFile = Request.Files["img_upload"];

                    using (var binary = new BinaryReader(poImgFile.InputStream))
                    {
                        Pic = binary.ReadBytes(poImgFile.ContentLength);
                    }
                }
                manual.pdf = Pic;
                db.Manual.Add(manual);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(manual);
        }
        public FileResult OpenPDFIdDoc(int id)
        {
            var fl = db.Manual.Where(l => l.Id == id).Select(o => o.pdf).FirstOrDefault();
            byte[] pdfByte = fl;
            return File(pdfByte, "application/pdf");
        }


        // GET: Manuals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Manual manual = db.Manual.Find(id);
            if (manual == null)
            {
                return HttpNotFound();
            }
            return View(manual);
        }

        // POST: Manuals/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PdfName,pdf")] Manual manual)
        {
            if (ModelState.IsValid)
            {
                db.Entry(manual).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(manual);
        }

        // GET: Manuals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Manual manual = db.Manual.Find(id);
            if (manual == null)
            {
                return HttpNotFound();
            }
            return View(manual);
        }

        // POST: Manuals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Manual manual = db.Manual.Find(id);
            db.Manual.Remove(manual);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
